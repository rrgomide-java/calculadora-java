package br.com.rrgomide.interfaces;

/*
 * Interface que cria um contrato
 * para quem quiser implementá-la
 */
public interface Multipliable {

  int multiply(int operand1, int operand2);
}
