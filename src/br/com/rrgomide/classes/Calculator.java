package br.com.rrgomide.classes;

import br.com.rrgomide.interfaces.Divisible;
import br.com.rrgomide.interfaces.Multipliable;

/*
 * Classe principal deste programa
 */
public class Calculator {
  
  /*
   * Os dois operadores a serem
   * preenchidos pelo usuário
   */
  private int operand1;
  private int operand2;

  /*
   * Para o multiplicador e o divisor, criei
   * como interface, e não como classe. Isso
   * permite utilizarmos técnicas de polimorfismo.
   * Na criação do objeto, podemos definir qual
   * multiplicador e qual divisor será utilizado.
   */
  private Multipliable multiplicator;
  private Divisible divisor;
  
  /*
   * Construtor, que foi implementado para proteger a classe
   */
  public Calculator(int operand1, int operand2, Multipliable multiplicator, Divisible divisor) {

    /*
     * Validando números negativos
     */
    if(operand1 < 0 || operand2 < 0)
      throw new IllegalArgumentException("Operands must be whole numbers greater than 0!");
    
    /*
     * Validando multiplicador ou divisor nulos
     */
    if(multiplicator == null || divisor == null)
      throw new IllegalArgumentException("You must provide valid implementations of multiplicator and divisor objects!");
    
    /*
     * Após a validação, atribuímos
     * os valores
     */
    this.operand1 = operand1;
    this.operand2 = operand2;
    this.multiplicator = multiplicator;
    this.divisor = divisor;
  }
  
  /*
   * Seção de getters para os operandos
   */
  public int getOperando1() {
    return operand1;
  }
  
  public int getOperando2() {
    return operand2;
  }
  
  /*
   * Cálculo de soma
   */
  public int sum() {
    return operand1 + operand2;
  }
  
  /*
   * Cálculo de subtração
   */  
  public int subtract() {
    return operand1 - operand2;
  }
  
  /*
   * Cálculo de multiplicação, que funciona
   * independente da implementação escolhida
   * (já que segue o contrato da interface
   */  
  public int multiply() {
    return multiplicator.multiply(this.operand1, this.operand2);
  }

  /*
   * Cálculo de divisão, que funciona
   * independente da implementação escolhida
   * (já que segue o contrato da interface
   */    
  public int divide() {
    return divisor.divide(this.operand1, this.operand2);
  }
  
  /*
   * Método que centraliza todas as operações
   * da calculadora e exibe um resultado 
   * mais amigável ao usuário
   */
  public void executeAllCalculations() {
    
    System.out.println("\nCalculator");
    System.out.println("==========");
    System.out.println("");
    System.out.println(String.format("Sum: (%d + %d = %d)", operand1, operand2, sum()));    
    System.out.println(String.format("Subtraction: (%d - %d = %d)", operand1, operand2, subtract()));    
    System.out.println(String.format("Multiplication: (%d x %d = %d)", operand1, operand2, multiply()));  
    
    int division = 0;
    
    /*
     * Aqui validamos a divisão por 0,
     * caso aconteça
     */
    try {
      division = divide();      
      System.out.println(String.format("Division: (%d ÷ %d = %d)", operand1, operand2, divide()));    
    }
    catch (IllegalArgumentException e) {
      
      System.out.println(e.getMessage());
    }
    catch (Exception e) {
      
      System.out.println("Unknown error: " + e.getMessage());
    }   
    
    System.out.println("");
    System.out.println("");
  }
}
