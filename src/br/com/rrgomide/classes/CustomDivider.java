package br.com.rrgomide.classes;

import br.com.rrgomide.interfaces.Divisible;

/*
 * Aqui faço a implementação customizada da divisão,
 * implementando a interface Divisible 
 */
public class CustomDivider implements Divisible {

  /*
   * Método da interface a ser implementado
   */
  @Override
  public int divide(int operand1, int operand2) {
    
    /*
     * Validando a divisão por zero
     */
    if (operand2 == 0)
      throw new IllegalArgumentException("Invalid argument! I can't calculate a division by zero!");
    
    int division = operand1;
    int quotient = 0;
    
    while (division > 0) {      
      division -= operand2;
      if (division >= 0)
        quotient++;
    }
    
    return quotient;    
  }
}
