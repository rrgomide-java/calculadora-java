package br.com.rrgomide.classes;

import br.com.rrgomide.interfaces.Multipliable;

/*
 * Aqui faço a implementação simples da multiplicação,
 * implementando a interface Multipliable 
 */
public class SimpleMultiplier implements Multipliable {

  /*
   * Método da interface a ser implementado
   */
  @Override
  public int multiply(int operand1, int operand2) {
    return operand1 * operand2;
  }
}
