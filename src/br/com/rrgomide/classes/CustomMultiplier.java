package br.com.rrgomide.classes;

import br.com.rrgomide.interfaces.Multipliable;

/*
 * Aqui faço a implementação customizada da multiplicação,
 * implementando a interface Multipliable 
 */
public class CustomMultiplier implements Multipliable {

  /*
   * Método da interface a ser implementado
   */
  @Override
  public int multiply(int operand1, int operand2) {
    
    int product = 0;
    
    for(int i = 1; i <= operand1; i++) 
      product += operand2;    
    
    return product;
  }
}
