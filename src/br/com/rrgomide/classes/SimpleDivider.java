package br.com.rrgomide.classes;

import br.com.rrgomide.interfaces.Divisible;

/*
 * Aqui faço a implementação simples da divisão,
 * implementando a interface Multipliable 
 */
public class SimpleDivider implements Divisible {

  @Override
  public int divide(int operand1, int operand2) {
    
    if (operand2 == 0)
      throw new IllegalArgumentException("Invalid argument! I can't calculate a division by zero!");
    
    return operand1 / operand2;
  }
}
