package br.com.rrgomide.tests;

import java.util.Scanner;

import br.com.rrgomide.classes.Calculator;
import br.com.rrgomide.classes.CustomDivider;
import br.com.rrgomide.classes.CustomMultiplier;
import br.com.rrgomide.classes.SimpleDivider;
import br.com.rrgomide.classes.SimpleMultiplier;
import br.com.rrgomide.interfaces.Divisible;
import br.com.rrgomide.interfaces.Multipliable;

public class CalculatorTest {
  
  static Multipliable simpleMultiplier = new SimpleMultiplier();
  static Divisible simpleDivider = new SimpleDivider();
  static Multipliable customMultiplier = new CustomMultiplier();
  static Divisible customDivider = new CustomDivider();

  public static void main(String[] args) {

    //fixedValuesTest();    
    testWithUserInput();
  }

  private static void fixedValuesTest() {
    Calculator c = new Calculator(5, 3, customMultiplier, customDivider);
    c.executeAllCalculations();
  }

  private static void testWithUserInput() {
    
    int op1, op2;
    boolean validInputs = false;
    Scanner sc = new Scanner(System.in);
    Calculator c = null;
    
    while(!validInputs) {
    
      System.out.print("Enter first operand: ");
      op1 = sc.nextInt();
      
      System.out.print("Enter second operand: ");
      op2 = sc.nextInt();
      
      try {
        
        c = new Calculator(op1, op2, customMultiplier, customDivider);        
        validInputs = true;        
      } 
      catch (IllegalArgumentException e) {
        
        System.out.println(e.getMessage() + "\n");        
      }
      catch(Exception e) {
        
        System.out.println("Unknown error: " + e.getMessage() + "\n");        
      }      
    }
    
    c.executeAllCalculations();    
    sc.close();
  }   
}
