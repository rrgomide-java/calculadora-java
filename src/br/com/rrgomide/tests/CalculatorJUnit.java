package br.com.rrgomide.tests;

import org.junit.Assert;
import org.junit.Test;

import br.com.rrgomide.classes.Calculator;
import br.com.rrgomide.classes.CustomDivider;
import br.com.rrgomide.classes.CustomMultiplier;
import br.com.rrgomide.interfaces.Divisible;
import br.com.rrgomide.interfaces.Multipliable;

public class CalculatorJUnit {

  private Multipliable customMultiplier = new CustomMultiplier();
  private Divisible customDivider = new CustomDivider();
  
  @Test
  public void testingWithFirstOperandGreaterThanSecondOneAndOddDivision() {
    Calculator c = new Calculator(29, 6, customMultiplier, customDivider);
    Assert.assertEquals(4, c.divide());
    Assert.assertEquals(174, c.multiply());       
  }
  
  @Test
  public void testingWithFirstOperandGreaterThanSecondOneAndEvenDivision() {
    Calculator c = new Calculator(30, 5, customMultiplier, customDivider);
    Assert.assertEquals(6, c.divide());
    Assert.assertEquals(150, c.multiply());       
  }

  @Test
  public void testingWithFirstOperandLowerThanSecondOneAndOddDivision() {
    Calculator c = new Calculator(6, 29, customMultiplier, customDivider);
    Assert.assertEquals(0, c.divide());
    Assert.assertEquals(174, c.multiply());       
  }
  
  @Test
  public void testingWithFirstOperandLowerThanSecondOneAndEvenDivision() {
    Calculator c = new Calculator(5, 30, customMultiplier, customDivider);
    Assert.assertEquals(0, c.divide());
    Assert.assertEquals(150, c.multiply());       
  }

  @Test
  public void testingWithFirstOperandEqualsToSecondOne() {
    Calculator c = new Calculator(6, 6, customMultiplier, customDivider);
    Assert.assertEquals(1, c.divide());
    Assert.assertEquals(36, c.multiply());       
  }  

  @Test(expected = IllegalArgumentException.class)
  public void testingDivisionBy0() {
    Calculator c = new Calculator(6, 0, customMultiplier, customDivider);
    c.divide();
  }  
}
